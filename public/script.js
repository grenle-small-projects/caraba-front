const env = "PROD"

const baseUrl = env === "DEV" ? "localhost:3003/v1/" : "https://caraba-api.onrender.com/v1/"

const requests = {
    getAll    : async () => await fetch(`${baseUrl}blagues`),
    getById   : async id => await fetch(`${baseUrl}blagues/${id}`),
    getRandom : async () => await fetch(`${baseUrl}blagues/random`, { mode: "cors" })
}

const Joke = {
    getRandom: async function refreshJoke(){
        const response = await requests.getRandom()
        const newJoke = await response.json()
        return newJoke
    }
}

function newJokeHandler({button, question, answer}){
    return async function(_event){
        button.disabled = true
        button.innerText = "Patience!"
        const joke = await Joke.getRandom()
        function fadein(_transitionEvent){
            question.removeEventListener("transitionend", fadein)
            button.disabled = false
            button.innerText = "Rafraîchir"
        }
        function fadeout(_transitionEvent){
            question.removeEventListener("transitionend", fadeout)
            question.innerText = `- ${joke.question}`
            answer.innerText = `- ${joke.answer}`
            question.style.opacity = 1
            answer.style.opacity = 1
            question.addEventListener("transitionend", fadein)
        }
        question.style.opacity = 0
        answer.style.opacity = 0
        question.addEventListener("transitionend", fadeout)
    }
}

const Control = ({button, question, answer}) => {
    button.addEventListener("click", newJokeHandler({button, question, answer}))
}

window.onload = _ => {
    const question = document.querySelector("#question")
    const answer = document.querySelector("#answer")
    if(question === null || answer === null){
        throw Error("Could not find question or answer elements")
    }
    const button = document.querySelector("#refresh-joke")
    if(button){
        newJokeHandler({button, question, answer})()
        Control({button, question, answer})
    }
    else{
        console.log("no button!")
    }
}
